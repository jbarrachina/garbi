
$(function()
{
    var arr = ["a", "b", "c", "d", "e"];
    $("div").text(arr.join(", "));

    arr = $.map(arr, function (n, i) {
        return (n.toUpperCase() + i);
    });
    $("p").text(arr.join(", "));

    arr = $.map(arr, function (a) {
        return a + a;
    });
    $("span").text(arr.join(", "));
});

