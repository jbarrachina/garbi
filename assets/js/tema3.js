/*
 * Manipulando los elementos del DOM
 */

$(function(){
/*
 * Cambiando el html
 */
   alert("contenido anterior: " + $('#contenido').html());
   //$('#contenido').html('<div>Dime de que presumes y te diré de ....</div>'); 
/*
 * Obteniendo el texto del objeto
 */   
   $("p.segundo").html($("p.primero").text());
/*
 * Añadiendo html dentro del elemento
 */   
    $('#contenido').append('<p>juego al fútbol 1 vez a la semana</p>');
    $('#contenido').prepend('<p>juego al pádel 1 vez a la semana</p>');
/*
 * Añadiendo html antes o después de un elemento
 */      
   /* var $obj = $("<p>Test 3</p>"); //no es texto, es un objeto del DOM
    var parrafo = document.createElement("p");
    parrafo.appendChild(document.createTextNode("Test 4"));
    $(".inner").before("<p>Test</p>", "<p>Test2</p>", $obj, parrafo);
    $('.container').after($('h2'));*/
/*
 * Reemplazando elementos
 */    
   //$('<h2>Saludos</h2>').replaceAll('.inner');
/*
 * Envolviendo Elementos
 */   
    $("p").wrapAll('<div class="all"></div>');
    $("p").wrapInner('<div class="inner"></div>');
    $("p").wrap('<div class="one"></div>');
});


