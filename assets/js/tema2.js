/* 
 * tema2.js
 */
﻿$(function() 
{
 /* 
 * Poner el color de fondo rojo a los <li> de una lista que, a su vez, contengan
 * otra lista: tema21
 */
    $("li").has("ul").css("background-color", "cyan");
 
/*
 * Verificar si una tabla contiene alguna celda vacía
 */ 
    
    if ($('table td').is('td:empty'))
        alert("Hay celda vacía");
    else
        alert("No hay celda vacía");
/*
 * Obtener una lista con los valores de los campos de texto existentes en un 
 * formulario
 */    
    $("p")
    .append(
        $("input").map(function ()
        {
            return $(this).val();
        })
        .get()
        .join(", ")
    );
/*
 * Poner un borde rojo a los párrafos que no tengan aplicado el estilo de clase 
 * sinresaltar :
 */
   $("p").not(".sinresaltar").css("border", "1px solid red");
/*
 * Poner la fuente de color rojo a todos los <span> que hayan dentro de un <p> :
 */     
   $("p").find("span").css("color", "red");
/*
 * Poner el color de fondo rojo a los siguientes hermanos de un elemento hasta 
 * que aparezca un hermano de tipo <dt> :
 */    
   $("#term-2").nextUntil("dt").css("background-color", "red");
/*
 * Poner el color de fondo rojo al padre de un elemento:
 */     
   $("li.item-a").parent().css("background-color", "red"); 
/*
 * En el siguiente ejemplo se muestran dos casos: el primero le pone el color 
 * de fondo amarillo a los párrafos que hay dentro de un <div>, mientras que
 * el segundo le pone el color de fondo amarillo a los párrafos que hay dentro
 * del <div> y también al propio <div> :
 */   
    $("div.izquierda,div.derecha").find("div,div > p").addClass("borde");
    // Primer ejemplo
    $("div.antes-addBack").find("p").addClass("fondo");
    // Segundo ejemplo
    $("div.despues-addBack").find("p").addBack().addClass("fondo");
/*
 * Poner el color de fondo rojo a los <li> de la clase coches y el color de fondo
 * verde a los <li> de la clase motos , cuando sean descendientes de un <ul> :
 */      
    $('ul')
    .find('li.coches')
    .css('background-color', 'red')
    .end()
    .find('li.motos')
    .css('background-color', 'green');
});


