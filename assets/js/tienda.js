$(document).ready( function () {
    $('.dataTable').DataTable();  
    
    $(".pictograma").mouseenter(function() {
        var sufijo = $(this).attr('id');
        $("#sound-" + sufijo )[0].play();
    });
} );

//https://codepen.io/waqasy/pen/rkuJf

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#visor')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}



