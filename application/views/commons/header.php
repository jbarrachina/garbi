<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables-1.10.18/css/dataTables.bootstrap4.min.css');?>"/>
        <script type="text/javascript" src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.18/js/jquery.dataTables.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.18/js/dataTables.bootstrap4.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/tienda.js');?>"></script>
        <title><?php echo $titulo; ?></title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <h1 class="text-white bg-danger col-12">
                    <?php echo $titulo; ?>
                    <a href="<?php echo site_url('articulo/carrito');?>">
                        <span class="fas fa-shopping-cart text-dark float-right"></span>
                    </a>
                    <span class="badge-dark float-right">
                        <?php echo isset($this->session->carrito) ? count($this->session->carrito) : 0 ; ?>
                    </span>
                </h1>
            </div> 

