<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
    <?php foreach($tablero as $fila): ?>
    <div class="row">
        <?php foreach($fila as $celda): ?>
            <div class="col-2">
                <div class="card">
                    <img src="<?php echo base_url('assets/tableros/clara/'.$celda->url_imagen);?>" class="card-img-top pictograma" title="<?php echo $celda->texto;?>" id="<?php echo $celda->fila,'-',$celda->columna;?>">
                    <audio preload="auto" id="sound-<?php echo $celda->fila,'-',$celda->columna;?>">    
                        <source src="<?php echo base_url('assets/tableros/clara/'.$celda->url_audio);?>" type="audio/mpeg">
                    </audio>
                </div>
            </div>    
        <?php endforeach; ?>
    </div>    
    <?php endforeach; ?>
</div>
