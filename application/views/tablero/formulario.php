<?php echo validation_errors();?>
<div class="bg-success">
    <?php echo $this->session->flashdata('exito');?>
</div>
<?php echo form_open_multipart(site_url('tablero/load'), ['class'=>'form-horizontal']);?>
<div class="row">
        <div class="col-12">
            <div class="custom-file">
                <?php echo form_upload(['name'=>'tablero','id'=>'tablero','class'=>'custom-file-control']);?>
                <?php echo form_label('Selecciona el tablero zip','tablero',['class'=>'custom-file-label']);?>
            </div>
        </div>
        <?php if (isset($error)): ?>
        <div class="col-12">
            <div class="bg-warning">
                <?php echo "Error: ".$error; ?>
            </div>
        </div>
        <?php endif; ?>
        <div class="col-12">
            <img id="visor" class=""  width="100%">
        </div>   
</div>    
<div class="form-row">
    <div class="col-12">
        <?php echo form_submit('enviar', 'Guardar');?>
    </div>   
</div>
<?php echo form_close();?>