<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<html>
    <head>
        <title>Todos los artículos</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo base_url('files/css/tienda.css');?>">    
    </head>    
    <body>
        <h1 class="text-center text-primary"><?php echo $titulo; ?></h1>
        <div class="container">
            <table class="table table-striped">
                <tr> 
                    <th>Imagen</th>
                    <th>Nombre</th>
                    <th>P.V.P. </th>
                </tr>
            <?php foreach ($articulos as $articulo): ?>
            <tr>
                <td><img src="<?php echo base_url('files/images/'.$articulo->codigo.'.jpg');?>" class="table-all"></td>
                <td><a href="<?php echo site_url('articulo/detalle/'.$articulo->id);?>"><?php echo $articulo->nombre;?></a></td>
                <td><?php echo $articulo->precio;?></td>
            </tr>
            <?php endforeach; ?>
            </table>
        </div>    
    </body>
</html>    

