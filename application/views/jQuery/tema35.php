<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
/*
 * Envolviendo Elementos
 * tema35
 */
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <style>
            p { padding:10px; }
            .one { border: green solid 1px; padding:10px; margin: 10px;}
            .all { border: blue solid 1px; padding:10px; margin: 10px;}
            .inner {border: red solid 1px; padding:10px; margin: 10px;}
        </style>    
        <title>jQuery: Ejemplo selector adyacente</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <link href="<?php echo base_url('assets/css/carro.css');?>" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/tema3.js');?>" type="text/javascript"></script>
    </head>
    <body>
        <h2>Saludos</h2>
        <p>Hola</p>
        <p>Adios</p>
    </body>
</html>    

