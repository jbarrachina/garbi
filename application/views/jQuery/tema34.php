<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
/*
 * Añadiendo html antes o después de un elemento
 * tema34
 */
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <style>
            .container{border:1px solid darkred;}
        </style>    
        <title>jQuery: Ejemplo selector adyacente</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <link href="<?php echo base_url('assets/css/carro.css');?>" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/tema3.js');?>" type="text/javascript"></script>
    </head>
    <body>
        <div class="container">
            <h2>Saludos</h2>
            <div class="inner">Hola</div>
            <div class="inner">Adios</div>
        </div>
    </body>
</html>    

