<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
/* 
 * oner un borde rojo a los párrafos que no tengan aplicado el estilo de 
 * clase sinresaltar :
 * tema25
 */
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>jQuery: Ejemplo selector adyacente</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <link href="<?php echo base_url('assets/css/carro.css');?>" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/tema2.js');?>" type="text/javascript"></script>
    </head>
    <body>
        <p class="sinresaltar">Esto es un párrafo de texto sin resaltar</p>
        <p>Esto es un párrafo de texto resaltado</p>
    </body>
</html>    

