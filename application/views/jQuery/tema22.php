<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
/* 
 * Poner el color de fondo rojo a los <li> de una lista que, a su vez, contengan
 * otra lista: tema21
 */
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>jQuery: Ejemplo selector adyacente</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <link href="<?php echo base_url('assets/css/carro.css');?>" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/tema2.js');?>" type="text/javascript"></script>
    </head>
    <body>
        <table>
            <tr>
                <th>Producto</th><th>Descripción</th><th>Precio</th>
            </tr>
            <tr>
                <td>Estuche de pinturas</td>
                <td>Pinturas de varios colores</td>
                <td>18,99 </td>
            </tr>
            <tr>
                <td>Compás</td>
                <td>De diferentes tamaños</td>
                <td> </td>
            </tr>
            <tr>
                <td>Folios</td>
                <td>Paquete de 500 folios</td>
                <td>5,99 </td>
            </tr>
            <tr>
                <td>Cartulina</td>
                <td>Tamaño din A2</td>
                <td></td>
            </tr>
        </table>
    </body>
</html>    

