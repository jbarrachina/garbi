<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>jQuery: Ejemplo selector adyacente</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <script src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.js');?>" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function()
            {
               $("div[id$='-entrada']").css(
                    {
                    "margin":"5px", "border":"1px solid #ccc",
                    "width": "300px", "background-color" : "lightblue"
                    });     
            });
        </script>        
    </head>
    <body>
        <div class="container">
            <div>
                <h1>Bienvenidos a jQuery</h1>
            </div>
            <div id="primera-entrada">
                Texto de la primera entrada
            </div>
            <div id="segunda-entrada">
                Texto de la segunda entrada
            </div>
            <div id="tercera-entrada">
                Texto de la tercera entrada
            </div>
            <div id="entrada-cuarta">
                Texto de la tercera entrada
            </div>
        </div>
    </body>
</html>


