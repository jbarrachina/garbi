<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
/*
 * En el siguiente ejemplo se muestran dos casos: el primero le pone el color 
 * de fondo amarillo a los párrafos que hay dentro de un <div>, mientras que
 * el segundo le pone el color de fondo amarillo a los párrafos que hay dentro
 * del <div> y también al propio <div> :
 * tema29
 */
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>jQuery: Ejemplo selector adyacente</title>
        <style>
            p, div { margin:5px; padding:5px; }
            .borde { border: 2px solid red; }
            .fondo { background:yellow; }
            .izquierda, .derecha { width: 45%; float: left;}
            .derecha { margin-left:3%; }
        </style>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <link href="<?php echo base_url('assets/css/carro.css');?>" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/tema2.js');?>" type="text/javascript"></script>
    </head>
    <body>
        <div class="izquierda">
            <p><strong>Antes <code>addBack()</code></strong></p>
            <div class="antes-addBack">
                <p>Primer párrafo</p>
                <p>Segundo párrafo</p>
            </div>
        </div>
        <div class="derecha">
            <p><strong>Después <code>addBack()</code></strong></p>
            <div class="despues-addBack">
                <p>Primer párrafo</p>
                <p>Segundo párrafo</p>
            </div>
        </div>
    </body>
</html>    

