<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
/*
 * Poner el color de fondo rojo a los <li> de la clase coches y el color de fondo
 * verde a los <li> de la clase motos , cuando sean descendientes de un <ul> :
 * tema30
 */
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>jQuery: Ejemplo selector adyacente</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <link href="<?php echo base_url('assets/css/carro.css');?>" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/tema2.js');?>" type="text/javascript"></script>
    </head>
    <body>
        <ul>
            <li class="camiones">Camiones</li>
            <li class="coches">Coches</li>
            <li class="motos">Motos</li>
            <li class="furgonetas">Furgonetas</li>
        </ul>
    </body>
</html>    

