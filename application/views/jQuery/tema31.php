<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
/*
 * Cambiando el html
 * tema31
 */
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>jQuery: Ejemplo selector adyacente</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <link href="<?php echo base_url('assets/css/carro.css');?>" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/tema3.js');?>" type="text/javascript"></script>
    </head>
    <body>
        <div id="contenido">
            <p>Corro 4 veces a la semana.</p>
            <p>Hago pesas 3 veces a la semana.</p>
        </div>
    </body>
</html>    

