<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
/* 
 * Poner el color de fondo rojo a los siguientes hermanos de un elemento 
 * hasta que aparezca un hermano de tipo <dt> :
 * tema27
 */
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>jQuery: Ejemplo selector adyacente</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <link href="<?php echo base_url('assets/css/carro.css');?>" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/tema2.js');?>" type="text/javascript"></script>
    </head>
    <body>
        <dl>
            <dt id="term-1">término 1</dt>
            <dd>definición 1-a</dd>
            <dd>definición 1-b</dd>
            <dd>definición 1-c</dd>
            <dd>definición 1-d</dd>
            <dt id="term-2">término 2</dt>
            <dd>definición 2-a</dd>
            <dd>definición 2-b</dd>
            <dd>definición 2-c</dd>
            <dt id="term-3">término 3</dt>
            <dd>definición 3-a</dd>
            <dd>definición 3-b</dd>
        </dl>
    </body>
</html>    

