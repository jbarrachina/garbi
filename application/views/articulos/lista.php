<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
        <title>Mi primera Vista</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <h1 class="text-white bg-danger col-12">
                    Lista de Artículos
                </h1>
            </div>    
            <div class="row">  
                <?php foreach ($resultado as $articulo): ?>
                <div class="col-sm-4">
                    <div class="card">
                        <img src="<?php echo base_url('assets/images/articles/'.$articulo->codigo.'.jpg');?>" class="card-img-top" alt="<?php echo $articulo->nombre; ?>">
                        <strong  class="text-center text-uppercase card-title bg-danger text-white"><?php echo $articulo->nombre; ?></strong>
                        <p class="card-body">
                            <?php echo substr($articulo->descripcion,0,100)." ..."; ?>
                             
                            <span style="color:blue"> ( <?php echo $articulo->volumen; ?> cl )</span>
                        </p>
                        <p class="text-right ">
                            <strong class="alert alert-info">
                                <?php echo $articulo->precio.' €'; ?>
                            </strong>
                        </p>    
                    </div>
                </div>
                <?php endforeach;?>
            </div>    
        </div>    
    </body>
</html>
