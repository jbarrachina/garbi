<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="offset-5 col-2">
        <a href="<?php echo site_url('articulo/alta');?>" class="btn btn-outline-info">
            Nuevo Artículo <span class="fas fa-plus-circle"></span>
        </a>
    </div>
</div>
    
<div class="row d-flex justify-content-center">  
    <table class="table table-striped table-condensed dataTable">
        <thead>
        <th class="col-md-1"></th>
        <th class="col-md-5">Artículo</th>
        <th class="col-md-1">Precio</th>
        <th class="col-md-1">Volumen</th>
        <th class="col-md-1">Categoría</th>
        <th class="col-md-3">Acciones</th>
        </thead>
        <tbody>
            <?php foreach ($resultado as $articulo): ?>
                <tr>
                    <td>
                        <img width="30px" src="<?php echo base_url('assets/images/articles/' . $articulo->codigo . '.jpg'); ?>" alt="<?php echo $articulo->nombre; ?>"> 
                    </td>
                    <td>
                        <?php echo $articulo->nombre; ?>
                    </td>
                    <td>
                        <?php echo $articulo->precio; ?>
                    </td>
                    <td>
                        <?php echo $articulo->volumen; ?>
                    </td>
                    <td>
                        <?php echo $articulo->categoria; ?>
                    </td>
                    <td>
                        <a href="<?php echo site_url('articulo/edita/' . $articulo->id); ?>" class="btn btn-sm btn-outline-info" title="Editar un registro" >
                            <span class="fas fa-edit"></span>
                        </a>
                        <a href="<?php echo site_url('articulo/borra/' . $articulo->id); ?>" class="btn btn-sm btn-outline-danger"  title="Borra un registro" onclick="return confirm('¿Estás seguro de borrar el artículo <?php echo $articulo->nombre; ?>')">
                            <span class="fas fa-trash"></span>
                        </a>
                        <a href="<?php echo site_url('articulo/compra/' . $articulo->id); ?>" class="btn btn-sm btn-outline-info"  title="Añade al carrito">
                            <span class="fas fa-cart-plus"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>    
    </table>
</div>    
   

