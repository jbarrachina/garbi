<?php echo validation_errors();?>
<div class="row">
<?php echo $this->session->flashdata('exito');?>
</div>    

<?php echo form_open_multipart(site_url('articulo/alta'), ['class'=>'form-horizontal']);?>
<div class="row">
    <div class="col-8">
        <div class="form-row">
            <div class="col-3">
                <?php echo form_label('Código:','codigo');?>
                <?php $marca = form_error('codigo')!== '' ? 'border-danger bg-warning':'';?>
                <?php echo form_input(['name'=>'codigo','id'=>'codigo', 'class'=>"form-control $marca", 'value'=>set_value('codigo')]);?>
                <?php echo form_error('codigo','<div class="small text-danger">','</div>');?>
            </div>
            <div class="col-9">
                <?php echo form_label('Nombre:','nombre');?>
                <?php echo form_input(['name'=>'nombre','id'=>'nombre', 'class'=>'form-control','placeholder'=>'Introduce el nombre del artículo', 'value'=>set_value('nombre')]);?>
            </div>    
        </div>  
        <div class="form-row">
            <div class="col-3">
                <?php echo form_label('Volumen:','volumen');?>
                <?php echo form_input(['name'=>'volumen','id'=>'volumen', 'class'=>'form-control', 'value'=>set_value('volumen',70)]);?>
            </div>
            <div class="col-3">
                <?php echo form_label('Precio:','precio');?>
                <?php $marca = form_error('precio')!== '' ? 'border-danger bg-warning':'';?>
                <?php echo form_input(['name'=>'precio','id'=>'precio', 'class'=>'form-control', 'value'=>set_value('precio')]);?>
                <?php echo form_error('precio','<div class="small text-danger">','</div>');?>
            </div>
            <div class="col-3">
                <?php echo form_label('Graduación:','grados');?>
                <?php echo form_input(['name'=>'grados','id'=>'grado', 'class'=>'form-control', 'value'=>set_value('grados')]);?>
            </div>
            <div class="col-3">
                <?php echo form_label('Categoría:','categoria');?>
                <?php echo form_dropdown('categoria', $categorias, set_value('categoria'), ['id'=>'categoria','class'=>'form-control']);?>
            </div>
        </div>
        <div class="form-row">
            <div class="col-12">
                <?php echo form_label('Descripción:','descripcion');?>
                <?php echo form_input(['name'=>'descripcion','id'=>'descripcion', 'class'=>'form-control', 'value'=>set_value('descripcion')]);?>
            </div>   
        </div> 
    </div> 
    <div class="col-4">
        <div class="col-12">
            <div class="custom-file">
                <?php echo form_upload(['name'=>'imagen','id'=>'imagen','class'=>'custom-file-control','onchange'=>'readURL(this);', 'style'=>'display:none;']);?>
                <?php echo form_label('Selecciona la imagen jpg','imagen',['class'=>'custom-file-label']);?>
            </div>
        </div>
        <?php if (isset($error)): ?>
        <div class="col-12">
            <div class="bg-warning">
                <?php echo "Error: ".$error; ?>
            </div>
        </div>
        <?php endif; ?>
        <div class="col-12">
            <img id="visor" class=""  width="100%">
        </div>
        
    </div>    
</div>    
<div class="form-row">
    <div class="col-12">
        <?php echo form_submit('enviar', 'Guardar');?>
    </div>   
</div>
<?php echo form_close();?>

