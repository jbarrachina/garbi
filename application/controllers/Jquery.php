<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jquery extends CI_Controller{

    public function index() {
        $this->load->view('jQuery/ejemplo');
    }
    
    public function carro() {
        $this->load->view('jQuery/carro');
    }
    
    public function tema2(){
       $this->load->view('jQuery/tema29'); 
    }
    public function tema3(){
       $this->load->view('jQuery/tema50'); 
    }
}
