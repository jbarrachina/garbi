<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tablero
 *
 * @author jose
 */
class Tablero extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->helper(['url','form']);
        $this->load->library(['zip','session']);
    }
    
    //formulario para subir un nuevo tablero .zip
    public function formulario(){
        $datos['titulo'] = 'Alta de Nuevos Tableros';
        $this->load->view('commons/header',$datos);
        $this->load->view('tablero/formulario');
    }
    
    public function load(){
        $config['upload_path'] = 'assets/tableros';
        $path = FCPATH."assets/tableros/";
        $config['allowed_types'] = 'zip';
        $config['overwrite'] = TRUE;
        $config['max_size']      = 2000; //kb
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('tablero')){
            echo '<pre>';
            print_r($this->upload->data());
            echo '</pre>';
            echo $path.'<br>';
            //fichero subido
            $file_name = $this->upload->data('file_name');
            $zip = new ZipArchive;
            $res = $zip->open($path.$file_name);
            if ($res === TRUE) {               
                // Extract file
                $zip->extractTo($path);
                $zip->close();
                echo 'Unzip! ';
            } else {
                echo $path.'failed! '.$res.$this->upload->data('file_name');
                print_r($this->upload->data());
            }
        } else {
            echo $this->upload->display_errors();
        }
    }
    
    public function show($tablero){
        $file = FCPATH."assets/tableros/$tablero/tablero_comunicacion.xml";
        if (file_exists($file)) {
           $content = utf8_encode(file_get_contents($file)); //codificación no UTF-8
           $xml = simplexml_load_string($content); //simplexml_load_file($file)
           $filas = $xml->cabecera->componentes['filas'];
           $columnas = $xml->cabecera->componentes['columnas'];
           $matriz = [];
           for ($i=0;$i<$filas*$columnas;$i++){
               $celda = new stdClass();
               $celda->url_imagen = $xml->celda[$i]->pictograma['imagen']; 
               $celda->url_audio = $xml->celda[$i]->pictograma['audio'];
               $celda->texto = $xml->celda[$i]->pictograma['texto']; 
               $celda->fila = intval($xml->celda[$i]->coordenadas['fila']); 
               $celda->columna = intval($xml->celda[$i]->coordenadas['columna']);
               $matriz[intval($xml->celda[$i]->coordenadas['fila'])][intval($xml->celda[$i]->coordenadas['columna'])] = $celda;
           }
           $datos['tablero'] = $matriz;
           $datos['titulo'] = $tablero;
           $this->load->view('commons/header',$datos);
           $this->load->view('tablero/tablero',$datos);
           
        }  else {
            echo "No está: [$file]";
        }
    }
}
