<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Clase controladora
 * Contendrá las funciones que gestionarán los diferentes eventos que se produzcan
 * en la aplicación web
 * Cada función será un controlador.
 *
 * @author jose
 */
class Articulo extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper(['url']);
        $this->load->model('tienda');
    }
    
    //creamos el controlador por defecto
    public function index(){       
        $data['resultado'] = $this->tienda->get_articulos();
        $this->load->view('articulos/lista',$data);
        /*echo '<pre>';
        print_r($resultado);
        echo '</pre>';*/
    }
    
    //Este es el nuevo controlador
    public function tabla(){
        $datos['titulo'] = 'Lista de Artículos';
        $this->load->view('commons/header',$datos);
        $data['resultado'] = $this->tienda->get_articulos();
        $this->load->view('articulos/tabla',$data);
        $this->load->view('commons/footer');
    }         
    
    public function alta(){
        $this->load->helper('form');
        //primer paso
        $this->load->library(['form_validation']);
        //establecer las reglas
        $this->form_validation->set_rules('codigo', 'código del artículo', 'required|is_unique[articulos.codigo]|integer|trim');
        $this->form_validation->set_rules('nombre', 'nombre del artículo', 'required|trim');
        $this->form_validation->set_rules('volumen', 'capacidad del artículo', 'required|integer');
        $this->form_validation->set_rules('grados', 'graduación del artículo', 'required|integer');
        $this->form_validation->set_rules('precio', 'precio del artículo', 'required|decimal|greater_than[2]');
        $this->form_validation->set_rules('categoria', 'categoria del artículo', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('descripcion', 'descripción del artículo', 'required');
//diferencia entre el "bien" y el "mal"        
        if($this->form_validation->run()===FALSE){
            $datos['titulo'] = 'Alta de Nuevos Artículos';
            //obtener datos de categorias
            $datos['categorias'] = $this->tienda->get_categorias();
            $this->load->view('commons/header',$datos);
            $this->load->view('articulos/ficha_alta');
        } else {
            $articulo = [
               'nombre' => $this->input->post('nombre'),
                'precio' => $this->input->post('precio'),
                'codigo' => $this->input->post('codigo'),
                'volumen' => $this->input->post('volumen'),
                'grados' => $this->input->post('grados'),
                'categoria' => $this->input->post('categoria'),
                'descripcion' => $this->input->post('descripcion'),
            ];
            $config['upload_path'] = 'assets/images/articles';
            $config['file_name']   = $articulo['codigo'].'.jpg';     
            $config['allowed_types'] = 'jpg';
            $config['max_size']      = 300; //kb
            $config['max_width']     = 1200;
            $config['max_height']    = 1600; //3x4
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('imagen'))
            //la subida sea correcta
            {    
                $this->tienda->guardar($articulo);
                $this->session->set_flashdata('exito', "<div class='bg-success'>Artículo: <strong>".$this->input->post('nombre')."</strong> añadido correctamente</div>");
                redirect(site_url('articulo/alta'));
            }
            else
            //error en la subida
            {
                $datos['error'] = "Error: ".$this->upload->display_errors(); 
                $datos['titulo'] = 'Alta de Nuevos Artículos: Error';
                //obtener datos de categorias
                $datos['categorias'] = $this->tienda->get_categorias();
                $this->load->view('commons/header',$datos);
                $this->load->view('articulos/ficha_alta', $datos);
               
            }    
            $this->load->view('commons/footer');
        }
    }  
    
    public function edita($id){
        $this->load->helper('form');
        //primer paso
        $this->load->library(['form_validation']);
        //establecer las reglas
        $this->form_validation->set_rules('codigo', 'código del artículo', 'required|integer|trim');
        $this->form_validation->set_rules('nombre', 'nombre del artículo', 'required|trim');
        $this->form_validation->set_rules('volumen', 'capacidad del artículo', 'required|integer');
        $this->form_validation->set_rules('grados', 'graduación del artículo', 'required|integer');
        $this->form_validation->set_rules('precio', 'precio del artículo', 'required|decimal|greater_than[2]');
        $this->form_validation->set_rules('categoria', 'categoria del artículo', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('descripcion', 'descripción del artículo', 'required');
//diferencia entre el "bien" y el "mal"        
        if($this->form_validation->run()===FALSE){
            $datos['titulo'] = 'Modificación de Artículos';
            //obtener datos de categorias
            $datos['categorias'] = $this->tienda->get_categorias();
            //obtener los valores del artículo a modificar
            $datos['articulo'] = $this->tienda->get_articulo($id);
            $datos['id'] = $id;
            $this->load->view('commons/header',$datos);
            $this->load->view('articulos/ficha_edita');
            $this->load->view('commons/footer');
        } else {
            $datos['titulo'] = 'Éxito: Artículo  cvc guardado';
            $this->load->view('commons/header',$datos);
            $articulo = [
               'nombre' => $this->input->post('nombre'),
                'precio' => $this->input->post('precio'),
                'codigo' => $this->input->post('codigo'),
                'volumen' => $this->input->post('volumen'),
                'grados' => $this->input->post('grados'),
                'categoria' => $this->input->post('categoria'),
                'descripcion' => $this->input->post('descripcion'),
            ];
            $this->tienda->actualizar($articulo,$id);
            $this->load->view('commons/footer');
        }
    }
    
    public function borra ($id){
        $this->tienda->delete($id);
        echo "<h1>El artículo ha sido borrado</h1>";
        redirect(site_url('articulo/tabla'));
    } 
    
    public function compra($id){
        //carrito será la variable de sesion. array asociativo
        if ($this->session->has_userdata('carrito')){
            //existe
            $carrito = $this->session->carrito;
            if (isset($carrito[$id])){
                //ya tengo el artículo en el carrito
                $carrito[$id]++;
            } else {
                //añadir el nuevo artículo
                $carrito[$id]=1;
            }
        } else {
            //hay que crearla
            $carrito = [];
            $carrito[$id] = 1; 
        }
        $this->session->set_userdata('carrito', $carrito);
        redirect(site_url('articulo/tabla'));
    }
    
    public function carrito(){
        echo "<pre>";
        print_r($this->session->carrito);
        echo "</pre>";
    }
     
}
